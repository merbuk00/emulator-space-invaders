/**
 *  @file    Video.cpp
 *  @author  Kumar Pillai
 *  @date    01/Jan/2017
 *  @version 1.0
 *
 *  @section DESCRIPTION
 *
 *  Initialises and renders the video.
 *
 */

#include "Video.h"

Video::Video()
{
}

Video::~Video()
{
}

/**
 *   @brief  Initialises the video.
 *
 *   @param  g is the Graphics ptr
 *   @param  m is the Memory ptr
 *   @return bool
 */
bool Video::init(Graphics* g, Memory* m)
{
    X = 20;
    Y = 40;
    gVideo = g;
    //pitch = gVideo->getBackbuffer()->pitch >> 2;

    videoMemory = m;
    videoMemoryPtr = m->getMemoryPtr();

    // Retro
    for (int i=0; i<32; i++)
    {
        color[i][0]  = 0x000000; // Black
        color2[i][0] = 0x444444; // Black

        if(i>=26 and i<=27)
        {
            color[i][1]  = 0xFF0000; // Red
            color2[i][1] = 0xAA0000; // Red
        }
        else if(i>=2 and i<=7)
        {
            color[i][1]  = 0x00FF00; // Green
            color2[i][1] = 0x00AA00; // Green
        }
        else
        {
            color[i][1]  = 0xFFFFFF; // White
            color2[i][1] = 0xAAAAAA; // White
        }
    }

    // Retro

    if(!videoOutlineFont.load("fonts/blitz.fon", 14))
        return false;

    return true;
}

/**
 *   @brief  Draw the screen.
 *
 *   @return void
 */
void Video::draw()
{
    gVideo->clear(0, 0, 0);

    drawOutlineFont("Space Invaders emulator started. Press ESCAPE to quit.", 0, 0, 255, 255, 255);

    copyScreen();

    // Draw a rectangle around the area where our emulated video output will go
    gVideo->drawLine(X-1, Y-1, X+SPIN_SCREEN_W+1, Y-1, 200, 200, 200);  // Top
    gVideo->drawLine(X-1, Y+SPIN_SCREEN_H, X+SPIN_SCREEN_W, Y+SPIN_SCREEN_H, 200, 200, 200); // Bottom
    gVideo->drawLine(X-1, Y-1, X-1, Y+SPIN_SCREEN_H, 200, 200, 200);  // Left
    gVideo->drawLine(X+SPIN_SCREEN_W, Y-1, X+SPIN_SCREEN_W, Y+SPIN_SCREEN_H, 200, 200, 200);  // Right
}

void Video::drawOutlineFont(char text[], int x, int y, int r, int g, int b)
{
    videoOutlineFont.draw(text, x, y, r, g, b, gVideo);
}

/**
 *   @brief  Deciphers the emulator screen memory and draws the pixels.
 *
 *   @return void
 */
void Video::copyScreen()
{
    // Cycle thru every byte in memory
    for (int j=0; j<224; j++)
    {
        int src = 0x2400 + (j << 5);
        int r1=0, g1=0, b1=0;
        int r2=0, g2=0, b2=0;
		int k = 0;

        for (int i=0; i<32; i++)
        {
            Byte vram = videoMemoryPtr[src++];

            for (int x=0; x<8; x++)
            {
                // Simulate colour effect(red, green blue); the original consoles had colour transparencies
                // on the screen as rows to give the illusion of colour as the only screen colour was white.
                r1 = ((color [i][vram&1]) & 0xFF0000) >> 16;
                r2 = ((color2[i][vram&1]) & 0xFF0000) >> 16;
                g1 = ((color [i][vram&1]) & 0x00FF00) >> 8;
                g2 = ((color2[i][vram&1]) & 0x00FF00) >> 8;
                b1 = ((color [i][vram&1]) & 0x0000FF);
                b2 = ((color2[i][vram&1]) & 0x0000FF);

                gVideo->drawPixel(X+(j*2), Y+SPIN_SCREEN_H-k-1, r1, g1, b1);  // upright view
 				gVideo->drawPixel(X+(j*2), Y+SPIN_SCREEN_H-k-2, r2, g2, b2);

 				//gVideo->drawPixel(X+SPIN_SCREEN_W-1-(j*2), Y+SPIN_SCREEN_H-k-1, r1, g1, b1);  // upright view
 				//gVideo->drawPixel(X+SPIN_SCREEN_W-1-(j*2), Y+SPIN_SCREEN_H-k-2, r2, g2, b2);  // reversed

				k += 2;
                vram = vram >> 1;
            }
        }
    }
}

/*
/**
 *   @brief  For debugging.
 *
 *   @return void
 */

/*
void Video::copyScreen()
{
    for (int j=0; j<224; j++)
    {
        int src = 0x2400 + (j << 5);
		int k = 0;

        for (int i=0; i<32; i++)
        {
            Byte vram = videoMemoryPtr[src++];

            for (int x=0; x<8; x++)
            {
                int r=0, g=0, b=0;

                if (vram & 0x01)
                {
                    r=0xAA;
                    g=0;
                    b=0;
                }

 				//Video->drawPixel(X+k, Y+j, r, g, b);  // side view
 				gVideo->drawPixel(X+j, Y+SPIN_SCREEN_H-k-1, r, g, b);  // upright view
				k++;
                vram = vram >> 1;
            }
        }
    }
}
*/

/**
 *   @brief  Free video
 *
 *   @return void
 */
void Video::free()
{
    videoOutlineFont.free();
}

/**
 *   @brief  Get the ptr to the outline font object.
 *
 *   @return OutlineFont ptr
 */
OutlineFont* Video::getVideoOutlineFont()
{
    return &videoOutlineFont;
}
