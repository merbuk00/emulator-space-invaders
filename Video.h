/**
 *  @file    Video.h
 *  @author  Kumar Pillai
 *  @date    01/Jan/2017
 *  @version 1.0
 *
 *  @section DESCRIPTION
 *
 * CPU class header.
 *
 */

#ifndef VIDEO_H
#define VIDEO_H

#include <fstream>
#include "Core/Graphics.h"
#include "Core/OutlineFont.h"
#include "Memory.h"

const int SPIN_SCREEN_W = 224*2;
const int SPIN_SCREEN_H = 256*2;

class Video
{
private:
    int X;
    int Y;

    Graphics* gVideo;
    OutlineFont videoOutlineFont;
    Memory* videoMemory;
    Byte* videoMemoryPtr;

    // Retro
    int color[32][2];
    int color2[32][2];
    // Retro
public:
    Video();
    ~Video();

    bool init(Graphics* g, Memory* m);
    void draw();
    void drawOutlineFont(char text[], int x, int y, int r, int g, int b);
    void copyScreen();
    void free();

    OutlineFont* getVideoOutlineFont();
};

#endif

