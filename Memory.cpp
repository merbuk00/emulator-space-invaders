#include <fstream>
#include <cstring>
#include "Memory.h"

Memory::Memory()
{

}

Memory::~Memory()
{

}

bool Memory::init(char romFileSpec[])
{
    memoryPtr = new Byte[16384]();
    if (memoryPtr == NULL)
        return false;

    // Load in the SI game from ROM file.
    FILE *in;
    in = fopen(romFileSpec, "rb");
    fread(memoryPtr, 8192, 1, in);
    fclose(in);

    return true;
}

Byte* Memory::getMemoryPtr()
{
    return memoryPtr;
}

void Memory::free()
{
    if(memoryPtr != NULL)
    {
        delete[] memoryPtr;
    }
}
